package com.example.senalespasto;


import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

public class ConvivenciaActivity extends AppCompatActivity {

    Button btn_cuidados;
    Button btn_kit;
    Button btn_seguridad;
    Button btn_seniales;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convivencia);
        setTitle(R.string.tit_convivencia);

        btn_cuidados = (Button)findViewById(R.id.btn_conv_cuidados);
        btn_cuidados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(ConvivenciaActivity.this, PlayerActivity.class);
                Bundle b = new Bundle();
                b.putString("video", "cuidados");
                it1.putExtras(b);
                startActivity(it1);
            }
        });


        btn_kit = (Button)findViewById(R.id.btn_conv_kit);
        btn_kit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(ConvivenciaActivity.this, PlayerActivity.class);
                Bundle b = new Bundle();
                b.putString("video", "kit");
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btn_seguridad = (Button)findViewById(R.id.btn_conv_seguridad);
        btn_seguridad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(ConvivenciaActivity.this, PlayerActivity.class);
                Bundle b = new Bundle();
                b.putString("video", "seguridad");
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btn_seniales = (Button)findViewById(R.id.btn_conv_seniales);
        btn_seniales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(ConvivenciaActivity.this, PlayerActivity.class);
                Bundle b = new Bundle();
                b.putString("video", "seniales");
                it1.putExtras(b);
                startActivity(it1);
            }
        });

    }
}
