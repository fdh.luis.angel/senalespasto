package com.example.senalespasto;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class PyPCalendar {

    private String strDia, strMes, strAnio, strFecha;
    private String[] yearPyp;
    private long[] yearEvents;
    private int initPyp;
    static int DAY=0, MONTH=1, DATE=2, HOUR=3, YEAR=5;
    private int cDay, cMonth, cYear;


    public PyPCalendar() {
        initPyp=4;
        Calendar c = Calendar.getInstance();
        cDay = c.get(Calendar.DAY_OF_YEAR);
        cMonth = c.get(Calendar.MONTH);
        cYear = c.get(Calendar.YEAR);

        strDia = translate_day(getDate()[DAY]);
        strMes = translate_month(getDate()[MONTH]);
        strAnio = getDate()[YEAR];
        strFecha = c.get(Calendar.DAY_OF_MONTH)+"";
        this.fillPypYear(c.getActualMaximum(Calendar.DAY_OF_YEAR));
    }

    private void fillPypYear(int yDays){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_YEAR, 1);
        c.set(Calendar.HOUR,7);
        c.set(Calendar.MINUTE, 0);

        int placa=initPyp;
        this.yearPyp = new String[yDays];
        this.yearEvents = new long[yDays];
        for (int i=0;i<yDays; i++){
            c.set(Calendar.DAY_OF_YEAR,i+1);
            String[] match = c.getTime().toString().split(" ");
            if (translate_day(match[DAY]).compareTo("Domingo")!=0){
                yearPyp[i]=new String(placa+"-"+(placa+1));
                yearEvents[i] = c.getTimeInMillis();
                if (placa<=6){
                    placa+=2;
                }
                else{
                    placa=0;
                }
            }
            else{
                yearPyp[i]=new String("");
            }
            if(translate_day(match[DAY]).compareTo("Sábado")==0){
                yearPyp[i]="";
            }
        }
    }

    public String[] getDate() {
        Calendar calendar = Calendar.getInstance();
        String[] cd = calendar.getTime().toString().split(" ");
        return cd;
    }

    private String fix_hour(String hour){
        String []match = hour.split(":");
        hour = match[0]+":"+match[1];
        return hour;
    }

    public static String translate_month(String month){
        if (month.compareTo("Jan")==0){ month = "Enero"; }
        else if (month.compareTo("Feb")==0){ month = "Febrero"; }
        else if (month.compareTo("Mar")==0){ month = "Marzo"; }
        else if (month.compareTo("Apr")==0){ month = "Abril"; }
        else if (month.compareTo("May")==0){ month = "Mayo"; }
        else if (month.compareTo("Jun")==0){ month= "Junio"; }
        else if (month.compareTo("Jul")==0){ month = "Julio"; }
        else if (month.compareTo("Aug")==0){ month = "Agosto"; }
        else if (month.compareTo("Sep")==0){ month = "Septiembre"; }
        else if (month.compareTo("Oct")==0){ month = "Octubre"; }
        else if (month.compareTo("Nov")==0){ month = "Noviembre"; }
        else{ month = "Diciembre"; }
        return month;
    }

    private String translate_day(String day){
        if (day.compareTo("Mon")==0){ day = "Lunes";}
        else if (day.compareTo("Tue")==0){ day = "Martes";}
        else if (day.compareTo("Wed")==0){ day = "Miercoles"; }
        else if (day.compareTo("Thu")==0){ day = "Jueves";}
        else if (day.compareTo("Fri")==0){ day = "Viernes"; }
        else if (day.compareTo("Sat")==0){ day = "Sábado"; }
        else{ day = "Domingo"; }
        return day;
    }

    /**
     * Pico y placa del dia actual en formato 2-3
     * si es dia sabado o domingo retorna No aplica
     * @return Pico y placa de hoy
     */
    public String getPyp(){
        if (strDia.compareTo("Sábado")!=0 && strDia.compareTo("Domingo")!=0){
            return yearPyp[cDay-1];
        }
        else{
            return "No aplica";
        }
    }

    /**
     * Pico y placa del dia actual en formato 2-3
     * si es dia sabado o domingo retorna No aplica
     * @return Pico y placa de hoy
     */
    public String getPyp(Boolean b){

        if (strDia.compareTo("Sábado")!=0 && strDia.compareTo("Domingo")!=0){
            String data = yearPyp[cDay-1].split("-")[0]+" - "+ yearPyp[cDay-1].split("-")[1];
            return data;
        }
        else{
            return "  ";
        }
    }

    /**
     * Recibe un string de la placa formato numero
     * y retorna formato numero-numero si no es sabado ni domingo
     * caso contrario retorna No puede transitar
     * @param placa
     * @return
     */
    public String getPyp(String placa){
        int p = Integer.parseInt(placa);
        if (p%2!=0){
            p--;
        }
        placa = p + "-"+(p+1);

        if (strDia.compareTo("Sábado")!=0 && strDia.compareTo("Domingo")!=0){
            if (placa.compareTo(yearPyp[cDay-1])==0){
                return "No puede transitar";
            }
        }
        return "Sin restricción";
    }

    public int getCurrPyp(){
        String data = yearPyp[cDay-1];
        if (data.length()>0){
            return Integer.parseInt(data.split("-")[0]);
        }
        return -1;
    }

    public String getPyp(Date date){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        String[] tiempo = c.getTime().toString().split(" ");
        if (translate_day(tiempo[DAY]).compareTo("Sábado")!=0 && translate_day(tiempo[DAY]).compareTo("Domingo")!=0){
            return "Placas: "+yearPyp[c.get(Calendar.DAY_OF_YEAR)-1];
        }
        else{
            return "No aplica";
        }
    }

    public String getNextPyp(int placa){
        if (placa%2!=0){
            placa--;
        }
        String strPlaca = placa + "-"+(placa+1);

        int days=1;
        for (int i=cDay; i<yearPyp.length; i++){
            if (yearPyp[i].compareTo(strPlaca)!=0){
                days++;
            }
            else{
                break;
            }
        }
        if (days==1){
            return "mañana";
        }
        return " en " + days + " dias";
    }

    public long getNextPypMilis(int placa, int hora, int minutos){
        if (placa%2!=0){
            placa--;
        }
        String strPlaca = placa + "-"+(placa+1);
        Calendar aux = Calendar.getInstance();
        Calendar c = new GregorianCalendar(aux.get(Calendar.YEAR), aux.get(Calendar.MONTH), aux.get(Calendar.DAY_OF_MONTH));
        for (int i = c.get(Calendar.DAY_OF_YEAR); i < yearPyp.length; i++) {
            if (strPlaca.compareTo(yearPyp[i])==0){
                c.set(Calendar.DAY_OF_YEAR, (i+1));
                break;
            }
        }

        int am_pm = Calendar.AM;
        if (hora>12){
            am_pm=Calendar.PM;
            hora = hora-12;
        }
        c.set(Calendar.AM_PM, am_pm);

        c.set(Calendar.HOUR,hora);
        c.set(Calendar.MINUTE, minutos);
        c.set(Calendar.SECOND,0);

        return c.getTimeInMillis();
        //return 1571080800000l;
    }

    public String milisToDate(long milis){
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(milis);
        return c.getTime().toString();
    }

    public long getEvent(int pos, int placa){
        if (placa%2!=0){
            placa--;
        }
        String strPlaca = placa + "-"+(placa+1);
        if (yearPyp[pos].compareTo(strPlaca)==0){
            return yearEvents[pos];
        }
        else return 0;
    }


    public String getStrDia() {
        return strDia;
    }

    public String getStrMes() {
        return strMes;
    }

    public String getStrAnio() {
        return strAnio;
    }

    public String getStrFecha() {
        return strFecha;
    }

    public int getcDay() {
        return cDay;
    }

    public void setInitPyp(int initPyp){
        this.initPyp = initPyp;
    }

    public String[] getYearPyp() {
        return yearPyp;
    }

    public long[] getYearEvents() {
        return yearEvents;
    }

    public int getColor(int placa){
        if (placa%2!=0){
            placa--;
        }

        switch (placa){
            case 0: return 0xFF00FF00;
            case 2:return 0xFF888888;
            case 4:return 0xFFFF0000;
            case 6:return 0xFFFFFF00;
            case 8:return 0xFF0000FF;
        }
        return 0xFFFF0000;
    }

    public int getCurrHour(){
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.HOUR_OF_DAY);
    }

    public int getCurrMinute(){
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.MINUTE);
    }

    public long getNextMinute(){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.MINUTE, c.get(Calendar.MINUTE)+1);
        c.set(Calendar.SECOND, 0);
        return c.getTimeInMillis();
    }


    public static void main(String[] args) {
        PyPCalendar pyp = new PyPCalendar();
        System.out.println(pyp.milisToDate(1561141980149l));
        System.out.println(pyp.getNextPyp(2));
    }

}
