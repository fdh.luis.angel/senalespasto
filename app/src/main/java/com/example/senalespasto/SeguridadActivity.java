package com.example.senalespasto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class SeguridadActivity extends AppCompatActivity {
    ImageView btnCinturon;
    ImageView btnAirbag;
    ImageView btnCasco;
    ImageView btnAlcohol;
    ImageView btnImpacto;
    ImageView btnSuenio;
    ImageView btnBicicleta;
    ImageView btnVelocidad;
    ImageView btnDrogas;
    ImageView btnEficiente;
    ImageView btnDistraccion;
    ImageView btnValores;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seguridad);

        setTitle(R.string.tit_seguridad);
        btnCinturon = (ImageView)findViewById(R.id.btnCinturon);
        btnCinturon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "CINTURÓN DE SEGURIDAD");
                b.putInt("imagen",R.mipmap.img_cinturon);
                b.putInt("info",R.string.str_info_cinturon);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnAirbag = (ImageView)findViewById(R.id.btnAirbag);
        btnAirbag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "EL AIRBAG");
                b.putInt("imagen",R.mipmap.img_airgab);
                b.putInt("info",R.string.str_info_airbag);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnCasco = (ImageView)findViewById(R.id.btnCasco);
        btnCasco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "EL CASCO");
                b.putInt("imagen",R.mipmap.img_casco);
                b.putInt("info",R.string.str_info_casco);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnAlcohol = (ImageView)findViewById(R.id.btnAlcohol);
        btnAlcohol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "EL ALCOHOL Y LA CONDUCCIÓN");
                b.putInt("imagen",R.mipmap.img_alcohol);
                b.putInt("info",R.string.str_info_alcohol);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnImpacto = (ImageView)findViewById(R.id.btnImpacto);
        btnImpacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "EL IMPACTO MEDIOAMBIENTAL DEL TRÁFICO");
                b.putInt("imagen",R.mipmap.img_ambiental);
                b.putInt("info",R.string.str_info_impacto);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnSuenio = (ImageView)findViewById(R.id.btnSuenio);
        btnSuenio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "EL SUEÑO");
                b.putInt("imagen",R.mipmap.img_suenio);
                b.putInt("info",R.string.str_info_suenio);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnBicicleta = (ImageView)findViewById(R.id.btnBicicleta);
        btnBicicleta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "LA CONDUCCIÓN DE BICICLETAS");
                b.putInt("imagen",R.mipmap.img_conduccion);
                b.putInt("info",R.string.str_info_bicicletas);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnVelocidad = (ImageView)findViewById(R.id.btnVelocidad);
        btnVelocidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "LA VELOCIDAD");
                b.putInt("imagen",R.mipmap.img_velocidad);
                b.putInt("info",R.string.str_info_velocidad);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnDrogas = (ImageView)findViewById(R.id.btnDrogas);
        btnDrogas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "LAS DROGAS Y LOS MEDICAMENTOS");
                b.putInt("imagen",R.mipmap.img_drogas);
                b.putInt("info",R.string.str_info_drogas);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnEficiente = (ImageView)findViewById(R.id.btnEficiente);
        btnEficiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "CONDUCCIÓN EFICIENTE");
                b.putInt("imagen",R.mipmap.img_eficiente);
                b.putInt("info",R.string.str_info_conduccion);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnDistraccion = (ImageView)findViewById(R.id.btnDistraccion);
        btnDistraccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "DISTRACCIONES AL VOLANTE");
                b.putInt("imagen",R.mipmap.img_distraccion);
                b.putInt("info",R.string.str_info_distraccion);
                it1.putExtras(b);
                startActivity(it1);
            }
        });

        btnValores = (ImageView)findViewById(R.id.btnValores);
        btnValores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(SeguridadActivity.this, DescSeguridadActivity.class);
                Bundle b = new Bundle();
                b.putString("titulo", "LOS VALORES EN LA VÍA PÚBLICA");
                b.putInt("imagen",R.mipmap.img_valores);
                b.putInt("info",R.string.str_info_valores);
                it1.putExtras(b);
                startActivity(it1);
            }
        });
    }
}
