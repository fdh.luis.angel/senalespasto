package com.example.senalespasto;


import android.content.res.Resources;


public class ListaDatos {
    private String descReglamentarias;
    private String descPreventivas;
    private String descInformativas;
    private String descTransitorias;
    //---------------------------------------
    private String[] descKitCarros={
            "Un gato hidráulico con capacidad para elevar el vehículo",
            "Una cruceta",
            "Dos señales de carretera en forma de triángulo en material reflectivo",
            "El botiquín de primeros auxilios debe contener como mínimo los siguientes elementos:\nAntisépticos, un elemento de corte, algodón, gasa estéril, esparadrapo o vendas adhesivas, Venda elástica, analgésicos, jabón.",
            "Un extintor (no se especifica el tamaño del mismo).",
            "Dos tacos para bloquear el vehículo.",
            "Caja de herramientas básica que como mínimo deberá contener: alicate, destornilladores, llave de expansión y llaves fijas.",
            "Llanta de repuesto ",
            "Linterna"
    };
    private String[] descKitMotos={
            "KKIT DE PRIMEROS AUXILIOS PARA MOTO.\n\n En su interior encontramos los útiles más habituales e imprescindibles como gasas, apósitos, esparadrapo, vendas adhesivas, tijeras, guantes PVC y una manta isotérmica",
            "KIT DE REPARACIÓN DE PINCHAZOS PARA MOTO.\n\n Específico para neumáticos sin cámara. Cuenta con tres pequeñas bombonas de CO2, un rácor con válvula, un tubo flexible, un cutter, cinco parches de sellado, dos herramientas especiales y un pegamento especial para pinchazos,",
            "KIT DE EMERGENCIA PARA MOTO.\n\n Muy importante en el caso de que suframos una avería es que seamos visibles en el margen de la carretera. Este kit incluye un triángulo reflectante y un chaleco de alta visibilidad.",
            "KIT DE SEGURIDAD PARA EL EQUIPAJE DE LA MOTO.\n\n Se trata de un candado con cable de acero y cierre con combinación secreta que permite asegurar las bolsas blandas además de otros objetos que queremos dejar atados a la moto por un breve espacio de tiempo en una parada ocasional."
    };

    private int[] imgReglamentarias;
    private int[] imgPreventivas;
    private int[] imgInformativas;
    private int[] imgTransotirias;
    //---------------------------------------
    private int[] imgKitCarros;
    private int[] imgKitMotos;

    public ListaDatos(Resources res) {
        this.descReglamentarias = res.getString(R.string.reglamentarias_descr);
        this.descPreventivas = res.getString(R.string.preventivas_descr);
        this.descInformativas = res.getString(R.string.informativas_descr);
        this.descTransitorias = res.getString(R.string.transitorias_descr);
        initImgReglamentarias();
        initImgPreventivas();
        initImgInformativas();
        initImgTransotirias();

        initImgKitCarros();
        initImgKitMotos();
    }

    private void initImgKitCarros(){
        imgKitCarros = new int[]{R.mipmap.kc1,R.mipmap.kc2,R.mipmap.kc3,R.mipmap.kc4,R.mipmap.kc5,R.mipmap.kc6,R.mipmap.kc7,
                R.mipmap.kc8,R.mipmap.kc9};
    }

    private void initImgKitMotos(){
        imgKitMotos = new int[]{R.mipmap.km1,R.mipmap.km2,R.mipmap.km3,R.mipmap.km4};
    }

    private void initImgReglamentarias(){
        imgReglamentarias = new int[]{R.mipmap.r1, R.mipmap.r2, R.mipmap.r3, R.mipmap.r4, R.mipmap.r5,
                R.mipmap.r6, R.mipmap.r7, R.mipmap.r8, R.mipmap.r9, R.mipmap.r10,
                R.mipmap.r11, R.mipmap.r12, R.mipmap.r13, R.mipmap.r14, R.mipmap.r15,
                R.mipmap.r16, R.mipmap.r17, R.mipmap.r18, R.mipmap.r19, R.mipmap.r20,
                R.mipmap.r21, R.mipmap.r22, R.mipmap.r23, R.mipmap.r24, R.mipmap.r25,
                R.mipmap.r26, R.mipmap.r27, R.mipmap.r28, R.mipmap.r29, R.mipmap.r30,
                R.mipmap.r31, R.mipmap.r32, R.mipmap.r33};
    }

    private void initImgPreventivas(){
        imgPreventivas = new int[]{R.mipmap.p1, R.mipmap.p2, R.mipmap.p3, R.mipmap.p4, R.mipmap.p5,
                R.mipmap.p6, R.mipmap.p7, R.mipmap.p8, R.mipmap.p9, R.mipmap.p10,
                R.mipmap.p11, R.mipmap.p12, R.mipmap.p13, R.mipmap.p14, R.mipmap.p15,
                R.mipmap.p16, R.mipmap.p17, R.mipmap.p18, R.mipmap.p19, R.mipmap.p20,
                R.mipmap.p21, R.mipmap.p22, R.mipmap.p23, R.mipmap.p24, R.mipmap.p25,
                R.mipmap.p26, R.mipmap.p27, R.mipmap.p28, R.mipmap.p29, R.mipmap.p30,
                R.mipmap.p31, R.mipmap.p32, R.mipmap.p33, R.mipmap.p34, R.mipmap.p35,
                R.mipmap.p36, R.mipmap.p37, R.mipmap.p38, R.mipmap.p39, R.mipmap.p40,
                R.mipmap.p41, R.mipmap.p42, R.mipmap.p43, R.mipmap.p44, R.mipmap.p45,
                R.mipmap.p46, R.mipmap.p47, R.mipmap.p48, R.mipmap.p49, R.mipmap.p50,
                R.mipmap.p51, R.mipmap.p52, R.mipmap.p53};
    }

    private void initImgInformativas(){
        imgInformativas = new int[]{R.mipmap.i1, R.mipmap.i2, R.mipmap.i3, R.mipmap.i4, R.mipmap.i5,
                R.mipmap.i6, R.mipmap.i7, R.mipmap.i8, R.mipmap.i9, R.mipmap.i10,
                R.mipmap.i11, R.mipmap.i12, R.mipmap.i13, R.mipmap.i14, R.mipmap.i15,
                R.mipmap.i16, R.mipmap.i17, R.mipmap.i18, R.mipmap.i19, R.mipmap.i20,
                R.mipmap.i21, R.mipmap.i22, R.mipmap.i23, R.mipmap.i24, R.mipmap.i25,
                R.mipmap.i26, R.mipmap.i27, R.mipmap.i28, R.mipmap.i29, R.mipmap.i30,
                R.mipmap.i31};
    }

    private void initImgTransotirias(){
        imgTransotirias = new int[]{R.mipmap.t1, R.mipmap.t2, R.mipmap.t3, R.mipmap.t4, R.mipmap.t5,
                R.mipmap.t6, R.mipmap.t7, R.mipmap.t8, R.mipmap.t9, R.mipmap.t10,
                R.mipmap.t11, R.mipmap.t12, R.mipmap.t13};
    }

    private  String [] reglamentarias = {
            "Ceda el paso:Señal indicadora de la posibilidad de darle el paso a aquellos vehículos que circular a mayor velocidad que otros.",
            "Contramano:El significado de esta señal es que el conductor no puede tomar esa calle",
            "Giro obligatorio:Esta señal indica al conductor que debe girar obligatoriamente hacia la dirección que esta indica, y no hacia otra.",
            "Límite de altura:Muy similar a la señal preventiva \"Altura limitada\", advierte al conductor la imposibilidad de pasar por ese trayecto si sobrepasa la altura que indica el cartel.",
            "Límite de ancho: Es de la misma familia de señales que indican altura y ancho permitida para pasar por determinados caminos.",
            "Límite de largo: Otra señal que indica en este caso el largo permitido que puede tener un vehículo para acceder a determinados lugares.",
            "Límite de peso: Esta señal es muy común en ciudades y advierte sobre el peso máximo que puede tener un vehículo, también es muy común encontrarlas en puentes.",
            "Límite de velocidad: De las más comunes de las señales de tránsito, la podemos encontrar en rutas y caminos o calles de ciudad.",
            "Prohibido avanzar: Como su nombre y diseño lo indica, prohíbe determinantemente el avance de cualquier vehículo.",
            "Prohibido estacionar: La podemos encontrar en hospitales, o instituciones públicas como escuelas o juzgados. También se la conoce como No estacionar",
            "Prohibido estacionar o detenerse: A diferencia de la anterior señal, está prohíbe a los conductores no sólo estacionar sino también a detenerse.",
            "Prohibido girar a la derecha: Esta señal prohíbe determinantemente a los conductores girar a la derecha.",
            "Prohibido girar a la izquierda: Similar a la anterior pero indicando que bajo ningún motivo se puede girar a la izquierda.",
            "Prohibido girar en U: Señal muy común y útil, la solemos encontrar en avenidas de varias manos, logrando así que se eviten colisiones.",
            "Prohibido ruidos molestos: La solemos encontrar en hospitales o lugares donde se necesitan un límite de ruido",
            "Pare o Alto: Otra de las señales más comunes, indicando esta la obligación de detener el vehículo. La solemos conocer también como \"Alto\"",
            "Paso obligado: Por lo general esta señal indica al conductor que debe circular obligatoriamente por el lugar indicado por la figura.",
            "Peatones por izquierda: Para evitar inconvenientes con el tránsito, muchas veces se obliga a los peatones a circular por determinados lugares, en este caso por la izquierda.",
            "Prohibido adelantarse: La solemos encontrar en calles o caminos donde estos son demasiado angostos o que son demasiado transitados.",
            "Prohibido cambiar de carril: Muy similar a la anterior señal, pero esta indica que no se puede cambiar de carril.",
            "Prohibido circular con animales: Esta señal también hace referencia a la circulación de vehículos que son de tracción a sangre.",
            "Prohibido circular autos: Como su nombre lo indica, queda determinantemente prohibido la circulación de vehículos por la zona indicada.",
            "Prohibido circular bicicletas: Esta señal restringe la circulación de bicicletas en el lugar donde se encuentra la misma.",
            "Prohibido circular camiones: Del mismo grupo de señales prohibitivas, esta hace referencia a vehículos de gran porte como puede ser los camiones.",
            "Prohibido circular motos: Hace referencia a la prohibición total para circular en motocicletas.",
            "Prohibido circular peatones: Espacio restringido para la circulación de peatones.",
            "Prohibido circular con tracción a sangre: Muy similar a la señal de tránsito \"Prohibido circular con animales\" pero refiriéndose a vehículos de tracción a sangre.",
            "Prohibido circular tractor: Esta señal de tránsito también hace referencia a la circulación de maquinarias agrícolas.",
            "Puesto de control: La solemos encontrar en puestos fronterizos, o cuando se está haciendo algún control vehicular.",
            "Avance: Señal que indica que no hay que parar ni detenerse",
            "Sentido circulación: Señal que indica en qué sentido se debe circular.",
            "Transporte pesado por la derecha: Micros, camiones, maquinarias agrícolas u otros vehículos de porte pesado deben circular por la derecha.",
            "Uso cadenas para la nieve: La imagen avisa del uso reglamentario de cadenas para las ruedas de los vehículos en caminos donde hay nieve.",
    };

    //private String descPreventivas = "Descripcion preventivas";
    private String [] preventivas = {
            "Acantilados:Ubicada en zonas costeras, éstas indican que hay que tomar precaución para evitar todo tipos caídas hacia el vacío",
            "Altura limitada o máxima:Esta señal está diseñada especialmente para aquellos vehículos de carga como camiones o que transportan una carga superior en altura a lo normal.",
            "Ancho limitado, máximo o permitido:Al igual que \"Altura limitada\" esta señal se refiere al ancho del vehículo, para así evitar que este se quede truncado.",
            "Ambulancia:Ubicada en hospitales o sanatorios clínicos para facilitar estacionamiento. Las sirenas encendidas le dan derecho de paso a las ambulancias. También indica entrada y salida de ambulancias.",
            "Animales sueltos:Alerta de presencia de animales en la vía, ubicada en zonas rurales. Se la puede encontrar con otra imagen como una vaca o alce. También puede indicar \"Animales libres\" o \"Animales en la vía\".",
            "Badenes o badén:Los badenes o badén son los desagotes de agua que atraviesan las rutas o calles. Es muy útil en zonas donde las lluvias son frecuentes. Se recomienda disminuir la velocidad.",
            "Bifurcación en Y:Muchas rutas y calles sufren lo que se llama bifurcación, un cambio de dirección o la apertura de una nueva ruta o camino.",
            "Bifurcación o empalme en T:Al igual que la bifurcación en V también tenemos la bifurcación o empalme en forma de T",
            "Calzada dividida o camino dividido:Como su nombre lo indica, esta señal indica una división en el camino, generado así 2 senderos de ida y vuelta.",
            "Calzada resbaladiza:Simboliza concretamente a sectores de la calzada que pueden generar peligro, como por ejemplo calzada de tierra o con zanjas.",
            "Camino en Zig Zag:Esta señalización vial significa que el camino es en zig zag y que hay que tener precaución",
            "Camino sinuoso:La imagen de esta señal muestra claramente el tipo de caminos al que nos enfrentamos que en su mayoría son con curvas y contra curvas, serpenteantes u ondulantes, muy común en caminos montañosos.",
            "Ciclistas en la vía:En la actualidad la podemos encontrar en ciudades donde existen sendas exclusivas para andar en bicicleta o triciclos. Esta señal también la conocemos como \"Ciclovía\"",
            "Cruce de caminos:Como su figura lo indica, 2 caminos que se cruzan alertan al conductor a tomar precauciones. También es conocida esta señal como \"Cruce peligroso de vías\".",
            "Cruce de trenes o ferroviario:Perteneciente a las señales ferroviarias, en este caso indicado presencia o cruce de vías de tren.",
            "Curva:Esta señal de tránsito es de las más conocidas, quedando en claro que indica la aproximación de una curva.",
            "Curva peligrosa hacia la izquierda: Este tipo de señales son muy eficaces a la hora de evitar derrapes o salidas de ruta estrepitosas.",
            "Curva peligrosa hacia la derecha: Igual que la anterior pero distinta dirección.",
            "Curva en S: Similar a la anterior señal de tránsito, sólo que esta hace alusión a un camino con forma de \"S\" con curvas y contracurvas.",
            "Zona de Derrumbes: Generalmente ubicada en zonas montañosas, pero también cuando se acumulan grandes cantidades de nieve.",
            "Empalmes laterales de izquierda y derecha: En este tipo la señal de tránsito indica un empalme. En este caso es de izquierda a derecha, pero también puede haber a la inversa.",
            "Empalme contrarios de derecha e izquierda: En este caso los empalmes de caminos van de derecha a izquierda",
            "Empalme contrarios de izquierda y derecha: A diferencia de la anterior, estos empalmes van de izquierda a derecha",
            "Corredor aéreo: El corredor aéreo es una ruta obligada en un determinado trayecto del vuelo de un avión, esta zona debe estar liberada para evitar problemas.",
            "Empalmes laterales de derecha: El empalme es por la derecha.",
            "Empalmes laterales de izquierda: Igual que las 2 anterior, pero el empalme es por la izquierda.",
            "Escuela cerca: Esta señal de tránsito es muy útil todas las instituciones educativas, especialmente donde concurren niños. Similares \"Despacio escuela\", \"Cuidado escuela\" o \"Zona de escuela\".",
            "Estrechamiento de calzada derecha: Señal muy importante en ocasiones donde los caminos están siendo reparados, o cuando el camino empalma con una ruta vieja y más angosta.",
            "Estrechamiento de calzada izquierda: Igual que la anterior pero el estrechamiento es por izquierda.",
            "Estrechamiento de calzada de ambas manos: A diferencia de las 2 anteriores, en este caso se estrechan ambas manos del camino.",
            "Exclusivo discapacitados: Esta señal indica la presencia o circulación de personas con ciertas limitaciones motrices.",
            "Exclusivo discapacitados: Esta señal indica la presencia o circulación de personas con ciertas limitaciones motrices.",
            "Indicador de semáforo: Esta señalización previene al conductor que está próximo a un semáforo",
            "Maquinaria agrícola: Indicador de presencia de tractores o maquinarias agrícolas",
            "Fuertes vientos laterales: Muy común encontrarlas en zonas geográficas donde el clima es extremo, muy tomada en cuenta por vehículos que transportan mercaderías a altas alturas.",
            "Ráfagas de viento: Tiene prácticamente el mismo uso que la anterior señal, y la solemos encontrar en aeródromos o aeropuertos pequeños.",
            "Puente angosto: Proximidad de un puente angosto",
            "Proyección de piedras: En muchos caminos podemos encontrar la existencia de gran cantidad de piedras sobre el pavimento, éstas pisarlas causan daños a otros vehículos o personas",
            "Vías de tren: Indica la proximidad de una vía de tran.",
            "Túnel: Indicador de aproximación de un túnel",
            "Pendiente ascendente: Esta señal indica que a metros el camino se vuelve ascendente, teniendo que tomar las precauciones correspondientes",
            "Pendiente descendente: Igual que la anterior pero en este caso es descendiente",
            "Perfil irregular: Esta señal indica que en las proximidades el camino se vuelve irregular",
            "Puente móvil: Indicador de aproximación de un puente móvil",
            "Tranvía: Indicador de aproximación de línea de tranvía",
            "Rotonda: Indicador de cercanía de una rotonda de circulación",
            "Proximidad de señal restrictiva: Indica la proximidad de una señal de tránsito, ésta puede ser de cualquiera tipo, por ejemplo PARE.",
            "Inicio de doble circulación, vía o sentido: Después de ir en un camino de una sola mano, podemos toparnos con un camino de doble circulación.",
            "Jinetes: Otra señal de tránsito correspondiente a zonas rurales, parajes o espacios de esparcimiento donde las personas suelen montar a caballos u otro tipo de animales.",
            "Lomo de burro o de toro: Una de las tantas señales que vemos a diario, en este caso indicando caminos o rutas con pequeñas lomas. Muchas de estas tienen la función de obligar al conductor a que conduzca a baja velocidad.",
            "Niños jugando: La solemos encontrar en espacio exclusivos para juegos o lugares públicos como plazas. También son conocidas como \"Niños en la vía\" o \"Niños cruzando\"",
            "Incorporación del tránsito lateral por derecha: Similar a la señal de tránsito \"Empalme\" o \"Bifurcación\" pero esta tiene la particularidad que el tráfico se incorpora en nuestra dirección.",
            "Incorporación del tránsito lateral por izquierda: Igual que la anterior pero desde otra dirección.",
    };

    //private String descInformativas = "Descripcion informativas";
    private String [] transitorias = {
            "Banderillero: Esta señal la encontramos comúnmente en las rutas o caminos que están siendo pavimentos o arreglados.",
            "Camino delineado: Muchas veces nos encontramos en rutas o caminos en construcción donde los sectores de paso están con montículos de tierra u otro material, obligando al conductor a tomar precauciones.",
            "Hombres trabajando: Advertencia de la existencia de trabajadores situados en la ruta o camino en construcción.",
            "Trabajo en banquina: Similar a \"Hombres trabajando\" esta se refiere a trabajadores situados en la banquina del camino.",
            "Conos: Si bien no es una señal de cartel, estos se utilizan generalmente para desviar el tránsito o crear zonas de exclusión.",
            "Delineadores: Similares a los conos en su función pero con distinta forma.",
            "Zonas explosivos: Señal muy utilizada en zonas mineras o de excavaciones varias.",
            "Fin de construcción: Esta señal declara el fin del trayecto en el cual se estuvo trabajando.",
            "Longitud de construcción: Indica la distancia o longitud que tiene un trayecto de ruta o camino en construcción.",
            "Vallas: Estas señales son muy utilizadas para separar espacios o crear senderos en los caminos.",
            "Estrechamiento de ambas manos: Es muy común que símbolos de un grupo de señales de tránsito funciones para otro grupo utilizando el color de este, por ejemplo lo vemos en las señales",
            "Estrechamiento de ambas manos: Es muy común que símbolos de un grupo de señales de tránsito funciones para otro grupo utilizando el color de este, por ejemplo lo vemos en las señales",
            "Estrechamiento de ambas manos: Es muy común que símbolos de un grupo de señales de tránsito funciones para otro grupo utilizando el color de este, por ejemplo lo vemos en las señales",
    };

    //private String descTransitorias = "Descripcion transitorias";
    private String []  informativas= {
            "Aeródromo o Aeropuerto: Esta señal de tránsito indica la cercanía o presencia de un aeropuerto o aeródromo.",
            "Autopista o autovía: Señal que nos indica que estamos conduciendo en una autopista o autovía, muchas veces nos indican el nombre de la misma.",
            "Comienzo de autopista o autovía: Señalización que nos indica el comienzo de una autopista o colectora cercana.",
            "Fin de autopista o autovía: Esta señal suele ser muy útil para tomar precauciones al entrar por ejemplo a zonas urbanas o de mucho tráfico.",
            "Balneario: Esta señal la solemos ver en ciudades turísticas, indicando así la cercanía de un balneario.",
            "Bar: Nos indica presencia de un bar o taberna. También puede indicar un paraje donde las personas pueden beber algo.",
            "Calle sin salida: Esta señal como dice su nombre indica que la calle no tiene salida, dando opciones de salidas posibles.",
            "Campamento o camping: Nos indica la presencia de un lugar para acampar, o para pasar el día al aire libre. Perteneciente a grupo de señales informativas sobre turismo.",
            "Servicio de Correo: Esta señal de tránsito nos muestra la cercanía o presencia de un correo o buzón de correo.",
            "Parada de micros o buses: Señalización de parada de micros o buses de larga o corta distancia, no se utiliza para transporte urbano. También puede referirse a una estación de buses o terminal de ómnibus.",
            "Discapacitados: Señal perteneciente también al grupo de señales preventivas, esta indica presencia o circulación de personas con limitaciones motrices.",
            "Estacionamiento de Motorhome: Esta señal de tránsito también es válida para casillas o casas rodantes indicando un lugar especial para estacionar este tipo de vehículos.",
            "Estación de servicio o gasolinera: Indica presencia o cercanía de una estación de servicio, muchas de ellas también poseen mercado de compras u otros servicios.",
            "Estacionamiento: Es una de las señales más conocidas e indica que está habilitado para estacionar vehículos. Pertenece también al grupo de señales de tránsito reglamentarias.",
            "Gomería: Presencia de lugar o comercio que se dedica al recambio, reparación o compra y venta de neumáticos y llantas.",
            "Hotel: Esta señal aparece muchas veces en ciudades turísticas o parajes donde las personas pueden utilizar servicios hoteleros, de hotel o hostel.",
            "Iglesia: Señal indicadora de la presencia de una iglesia o santuario.",
            "Museo: Dentro se las señales de tránsito referidas a servicios turísticos nos encontramos con esta señal que defne la presencia de un museo.",
            "Plaza o plazoleta: Indicación de la presencia de una plaza, plazoleta o espacio público donde también se pueden realizar varios tipos de actividades recreativas.",
            "Orientación: Esta señal de color verde y letras blancas da a las personas una noción de ubicación geográfica, también se ven estas señales con la cantidad de kilómetros de distancia a la ciudad indicada.",
            "Policía: Clara indicación de la presencia de una comisaría o destacamento policial. A veces indica la presencia de un policía de guardia.",
            "Puesto sanitario: Señal que hace alusión a un hospital o sala de emergencias fija o móvil.",
            "Punto panorámico: Esta señal es muy interesante, la misma hace referencia a lugares naturales o donde se tiene una privilegiada vista sobre el paisaje",
            "Restaurante: Indica la presencia o cercanía a un restaurant o local donde se pueden consumir alimentos. La podemos encontrar en zonas rurales o ciudad.",
            "Ruta Panamericana: Las rutas panamericanas son aquellas que circulan por varios países, como por ejemplo la Ruta Nacional 9 (Panamericana) que empieza en Buenos Aires y termina en Bolivia como Ruta",
            "Servicio mecánico: Este tipo de señales suelen aparecen en rutas o parajes. Podemos también encontrar servicios mecánicos particulares u oficiales de automotrices.",
            "Servicio telefónico: Hace referencia a lugares donde se puede tener acceso a hacer llamadas telefónicas, en algunos lugares se puede encontrar también con servicio WiFi (Internet).",
            "Parada de taxis: Al igual que la señal para buses, esta hace referencia a lugares públicos donde se puede acceder al servicio de un taxi.",
            "Teleférico: Siguiendo con las señales de tránsito que hacen referencia a transportes públicos, en este caso la señal indica acceso a teleféricos.",
            "Terminal o estación de ferrocarril: Otra señal sobre transporte público, una clara señalización sobre una estación de ferrocarril.",
            "Terminal de micros, ómnibus o buses: A diferencia de la otra señal sobre parada de buses, esta indica una estación o terminal de micros.",
    };



    public String[] getReglamentarias() {
        return reglamentarias;
    }

    public String[] getPreventivas() {
        return preventivas;
    }

    public String[] getInformativas() {
        return informativas;
    }

    public String[] getTransitorias() {
        return transitorias;
    }

    public String getDescReglamentarias() {
        return descReglamentarias;
    }

    public String getDescPreventivas() {
        return descPreventivas;
    }

    public String getDescInformativas() {
        return descInformativas;
    }

    public String getDescTransitorias() {
        return descTransitorias;
    }

    public int[] getImgReglamentarias() {
        return imgReglamentarias;
    }

    public int[] getImgPreventivas() {
        return imgPreventivas;
    }

    public int[] getImgInformativas() {
        return imgInformativas;
    }

    public int[] getImgTransotirias() {
        return imgTransotirias;
    }

    public String[] getDescKitCarros() {
        return descKitCarros;
    }

    public String[] getDescKitMotos() {
        return descKitMotos;
    }

    public int[] getImgKitCarros() {
        return imgKitCarros;
    }

    public int[] getImgKitMotos() {
        return imgKitMotos;
    }
}
