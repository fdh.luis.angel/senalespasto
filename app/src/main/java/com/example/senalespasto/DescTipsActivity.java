package com.example.senalespasto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class DescTipsActivity extends AppCompatActivity {
    ListaDatos datos;
    ListView list_tips_info;
    TextView lbl_titulo_desc_tips;
    TextView lbl_info_desc_tips;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desc_tips);
        setTitle("Transitic");
        final Bundle bundle = getIntent().getExtras();
        final int titulo = bundle.getInt("titulo");
        final String vehiculo = bundle.getString("vehiculo");
        final int info = bundle.getInt("info");

        lbl_titulo_desc_tips = (TextView)findViewById(R.id.lbl_titulo_desc_tips) ;
        lbl_titulo_desc_tips.setText(titulo);

        lbl_info_desc_tips = (TextView)findViewById(R.id.lbl_info_desc_tips) ;
        lbl_info_desc_tips.setText(info);

        datos  =  new ListaDatos(getResources());
        list_tips_info = (ListView)findViewById(R.id.list_tips_info);
        int size = 0;
        int height = 0;

        if (vehiculo.compareTo("motos")==0){
            list_tips_info.setAdapter(new TipsAdapter(this, datos.getDescKitMotos(), datos.getImgKitMotos()));
            size = datos.getDescKitMotos().length;
            height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 320, getResources().getDisplayMetrics());
        }
        else{
            list_tips_info.setAdapter(new TipsAdapter(this, datos.getDescKitCarros(), datos.getImgKitCarros()));
            size = datos.getDescKitCarros().length;
            height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());
        }

        ViewGroup.LayoutParams params = list_tips_info.getLayoutParams();
        params.height = height*size;
        list_tips_info.setLayoutParams(params);
    }
}
