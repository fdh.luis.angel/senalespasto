package com.example.senalespasto;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;


public class AlarmSoundService extends Service {

    private int NOTIFICATION = R.string.local_service_started;
    MediaPlayer media_song;
    int startId;
    private boolean isRunnimg=false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        String state = intent.getExtras().getString("extra");

        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);

        this.isRunnimg = false;

        assert state != null;
        switch (state){
            case "alarm on":
                this.startId = 1;
                break;
            case "alarm off":
                this.startId = 0;
                this.isRunnimg = true;
                break;
            default:
                this.startId = 0;
                break;
        }

        if (!this.isRunnimg && this.startId ==1){
            media_song = MediaPlayer.create(this, R.raw.tono_alarma);
            media_song.start();

            this.isRunnimg = true;
            this.startId = 0;

            Intent intent_alarm_activity = new Intent(this.getApplicationContext(), AlarmaActivity.class);
            //Bundle b = new Bundle();
            PendingIntent pendingIntent_al_activ = PendingIntent.getActivity(this, 0, intent_alarm_activity,0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                Notification notification_popup = new Notification.Builder(this)
                        .setContentTitle("Alarm is going off!")
                        .setContentText("Click aquí")
                        .setContentIntent(pendingIntent_al_activ)
                        .setSmallIcon(R.mipmap.r18)
                        .setAutoCancel(true)
                        .build();

                notificationManager.notify(0,notification_popup);
            }

        }
        else if (this.isRunnimg && this.startId ==0){
            media_song.stop();
            media_song.reset();

            this.isRunnimg = false;
            this.startId = 0;
        }
        else if (!this.isRunnimg && this.startId ==0){
            this.isRunnimg = false;
            this.startId = 0;
        }
        else if (this.isRunnimg && this.startId == 1){
            this.isRunnimg = true;
            this.startId = 1;
        }
        else{

        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.

        // Tell the user we stopped.
        Toast.makeText(this, R.string.local_service_stopped, Toast.LENGTH_SHORT).show();
    }
}
