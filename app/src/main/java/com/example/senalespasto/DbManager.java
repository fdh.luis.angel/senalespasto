package com.example.senalespasto;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class DbManager {
    public static final String TABLE_NAME="vehiculos";
    public static final String C_ID = "_id";
    public static final String C_NOMBRE = "nombre";
    public static final String C_PLACA = "placa";
    public static final String C_ALARMA = "alarma";
    public static final String CREATE_TABLE = " create table "+TABLE_NAME + " ("
            + C_ID + " integer primary key autoincrement,"
            + C_NOMBRE + " text not null,"
            + C_PLACA + " text,"
            + C_ALARMA + " text);";
    private DbHelper helper;
    private SQLiteDatabase db;

    private ArrayList<String> str_ids;
    private ArrayList<String> str_vehiculos;
    private ArrayList<String> str_placas;
    private ArrayList<String> str_alarmas;


    public DbManager(Context context) {
        helper = new DbHelper(context);
        db = helper.getWritableDatabase();

        str_ids = new ArrayList<>();
        str_vehiculos = new ArrayList<>();
        str_placas = new ArrayList<>();
        str_alarmas = new ArrayList<>();
    }

    public void insertar(String nombre, String placa, String alarma){
        String sentencia =  "INSERT INTO vehiculos (nombre, placa, alarma) values('"+nombre+"','"+placa+"','"+alarma+"');";
        db.execSQL(sentencia);
    }

    public void modificar(String id, String nombre, String placa, String alarma){
        String sentencia =  "UPDATE vehiculos SET nombre = '"+nombre+"', placa='"+placa+"', alarma = '"+alarma+"' where _id = "+id+";";
        db.execSQL(sentencia);
    }

    public void eliminar(String id){
        String sentencia =  "DELETE FROM vehiculos where _id = "+id+";";
        db.execSQL(sentencia);
    }

    public void consultar(){
        String sentencia =  "SELECT * FROM "+ TABLE_NAME;
        String [] campos = {C_ID, C_NOMBRE, C_PLACA,C_ALARMA};
        Cursor cursor = db.query(TABLE_NAME,campos,null,null,null,null,null);

        ArrayList<String> col1 = new ArrayList<>();
        ArrayList<String> col2 = new ArrayList<>();
        ArrayList<String> col3 = new ArrayList<>();
        ArrayList<String> col4 = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString( 0 );
                String vehiculo = cursor.getString( 1 );
                String placa = cursor.getString( 2 );
                String alarma = cursor.getString( 3 );

                col1.add(id);
                col2.add(vehiculo);
                col3.add(placa);
                col4.add(alarma);
            } while (cursor.moveToNext());
        }


        if (col1.size()>0){
            str_ids = col1;
            str_vehiculos = col2;
            str_placas = col3;
            str_alarmas = col4;
        }
        cursor.close();
    }


    public void close(){
        db.close();
    }

    public ArrayList<String> getStr_ids() {
        return str_ids;
    }

    public ArrayList<String> getStr_vehiculos() {
        return str_vehiculos;
    }

    public ArrayList<String> getStr_placas() {
        return str_placas;
    }

    public ArrayList<String> getStr_alarmas() {
        return str_alarmas;
    }
}
