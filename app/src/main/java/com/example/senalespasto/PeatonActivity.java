package com.example.senalespasto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PeatonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peaton);
        setTitle("NORMAS DE SEGURIDAD VIAL PARA LOS PEATONES");
    }
}
