package com.example.senalespasto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DescripcionActivity extends AppCompatActivity {

    TextView txt_titulo, txt_content_descrip;
    ImageView img_descrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion);

        Bundle bundle = getIntent().getExtras();

        String content_info = bundle.getString("descripcion");
        String titulo = content_info.split(":")[0];
        String descripcion = content_info.split(":")[1];

        int imagen = bundle.getInt("imagen");

        img_descrip = (ImageView)findViewById(R.id.img_descrip);
        if (imagen!=0)img_descrip.setImageResource(imagen);

        txt_titulo = (TextView)findViewById(R.id.txt_titulo_descrip);
        txt_titulo.setText(titulo);

        txt_content_descrip = (TextView)findViewById(R.id.txt_content_descrip);
        txt_content_descrip.setText(descripcion);
    }
}
