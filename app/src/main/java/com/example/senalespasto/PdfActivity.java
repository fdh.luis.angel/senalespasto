package com.example.senalespasto;

import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.listener.OnTapListener;

public class PdfActivity extends AppCompatActivity {
    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        pdfView = (PDFView)findViewById(R.id.pdf_viewer);
        pdfView.fromAsset("guia_practica.pdf")
                .password(null)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(true)
                .enableDoubletap(true)
                .onTap(new OnTapListener() {
                    @Override
                    public boolean onTap(MotionEvent e) {
                        return true;
                    }
                })
            .enableAnnotationRendering(true)
            .invalidPageColor(Color.WHITE)
            .load();
        pdfView.fitToWidth();
    }
}
