package com.example.senalespasto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ConductorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conductor);
        setTitle("NORMAS DE SEGURIDAD VIAL PARA LOS CONDUCTORES");
    }
}
