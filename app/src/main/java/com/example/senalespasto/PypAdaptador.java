package com.example.senalespasto;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PypAdaptador extends BaseAdapter {
    private static LayoutInflater inflater = null;
    Context contexto;
    private ArrayList<String> ids;
    private ArrayList<String> vehiculos;
    private ArrayList<String> placas;
    private ArrayList<String> alarmas;
    private PyPCalendar pyPCalendar;

    public PypAdaptador(Context contexto, ArrayList<String> vehiculos, ArrayList<String> placas, ArrayList<String> ids, ArrayList<String> alarmas) {
        this.contexto = contexto;
        this.vehiculos = vehiculos;
        this.placas = placas;
        this.ids = ids;
        this.alarmas = alarmas;
        pyPCalendar = new PyPCalendar();
        inflater = (LayoutInflater)contexto.getSystemService(contexto.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return vehiculos.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        final View vista = inflater.inflate(R.layout.elemento_vehiculo, null);

        final TextView lbl_hora = (TextView)vista.findViewById(R.id.lbl_elem_hora);
        final ImageView img_alarma = (ImageView) vista.findViewById(R.id.img_elem_alarma);
        if (alarmas.get(i).length()>1){
            img_alarma.setImageResource(R.mipmap.alarm_on_white);
            lbl_hora.setText(alarmas.get(i));
        }

        final ImageView img_tres_puntos = (ImageView)vista.findViewById(R.id.img_elem_trespuntos);
        final TextView lbl_nom_vehiculo =  (TextView) vista.findViewById(R.id.lbl_elem_nom_vehiculo);
        lbl_nom_vehiculo.setText(vehiculos.get(i));

        final TextView lbl_restriccion =  (TextView) vista.findViewById(R.id.lbl_elem_restriccion);
        lbl_restriccion.setText(pyPCalendar.getPyp(placas.get(i)));

        final TextView lbl_placa = (TextView)vista.findViewById(R.id.lbl_elem_placa);
        lbl_placa.setText(placas.get(i));
        if (lbl_restriccion.getText().toString().compareTo("No puede transitar")==0){
            lbl_placa.setBackgroundResource(R.drawable.bg_placa_red);
        }

        lbl_nom_vehiculo.setText(vehiculos.get(i));
        img_tres_puntos.setTag(i);

        img_tres_puntos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(contexto, "Imagen "+v.getTag(),Toast.LENGTH_SHORT).show();
                Intent it1 = new Intent(contexto, EditPypActivity.class);
                Bundle b = new Bundle();
                b.putString("nom_boton", "Eliminar");
                b.putString("id",ids.get((int)v.getTag()));
                b.putString("nom_vehiculo",vehiculos.get((int)v.getTag()));
                b.putString("placa",placas.get((int)v.getTag()));
                b.putString("alarma",alarmas.get((int)v.getTag()));
                it1.putExtras(b);
                contexto.startActivity(it1);
            }
        });

        return vista;
    }
}
