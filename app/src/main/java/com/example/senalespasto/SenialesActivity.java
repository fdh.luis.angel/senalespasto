package com.example.senalespasto;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;



public class SenialesActivity extends AppCompatActivity {

    Button btn_reglamentarias;
    Button btn_preventivas;
    Button btn_informativas;
    Button btn_transitorias;
    ListaDatos datos;
    /*
    * Se inicializan los listados de las imagenes
    * e informacion
    * */
    //Listado imagenes
    int[] imgReglamentarias ;
    int[] imgPreventivas ;
    int[] imgInformativas;
    int[] imgTransitorias;

    //Listado datos


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seniales);
        setTitle(R.string.tit_seniales);

        datos  =  new ListaDatos(getResources());

        imgReglamentarias = datos.getImgReglamentarias();
        imgPreventivas = datos.getImgPreventivas();
        imgInformativas = datos.getImgInformativas();
        imgTransitorias = datos.getImgTransotirias();

        btn_reglamentarias = (Button)findViewById(R.id.btn_reglamentarias);
        btn_reglamentarias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SenialesActivity.this, ListadoActivity.class);
                Bundle info = new Bundle();
                info.putString("titulo", "Señales reglamentarias");
                info.putString("descripcion", datos.getDescReglamentarias());
                info.putIntArray("imagenes",imgReglamentarias);
                info.putStringArray("datos", datos.getReglamentarias());
                it.putExtras(info);
                startActivity(it);
            }
        });

        btn_preventivas = (Button)findViewById(R.id.btn_preventivas);
        btn_preventivas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SenialesActivity.this, ListadoActivity.class);
                Bundle info = new Bundle();
                info.putString("titulo", "Señales preventivas");
                info.putString("descripcion", datos.getDescPreventivas());
                info.putIntArray("imagenes",imgPreventivas);
                info.putStringArray("datos", datos.getPreventivas());
                it.putExtras(info);
                startActivity(it);
            }
        });

        btn_informativas = (Button)findViewById(R.id.btn_informativas);
        btn_informativas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SenialesActivity.this, ListadoActivity.class);
                Bundle info = new Bundle();
                info.putString("titulo", "Señales informativas");
                info.putString("descripcion", datos.getDescInformativas());
                info.putIntArray("imagenes",imgInformativas);
                info.putStringArray("datos", datos.getInformativas());
                it.putExtras(info);
                startActivity(it);
            }
        });

        btn_transitorias = (Button)findViewById(R.id.btn_transitorias);
        btn_transitorias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(SenialesActivity.this, ListadoActivity.class);
                Bundle info = new Bundle();
                info.putString("titulo", "Señales transitorias");
                info.putString("descripcion", datos.getDescTransitorias());
                info.putIntArray("imagenes",imgTransitorias);
                info.putStringArray("datos", datos.getTransitorias());
                it.putExtras(info);
                startActivity(it);
            }
        });
    }
}
