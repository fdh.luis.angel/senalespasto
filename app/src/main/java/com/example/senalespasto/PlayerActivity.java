package com.example.senalespasto;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class PlayerActivity extends AppCompatActivity {
    VideoView videoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        setTitle("Transitic");

        final Bundle bundle = getIntent().getExtras();
        final String video = bundle.getString("video");

        videoPlayer = (VideoView)findViewById(R.id.videoPlayer);
        String videoPath = "android.resource://"+getPackageName()+"/";

        MediaController contrCuidados = new MediaController(this);
        videoPlayer.setMediaController(contrCuidados);
        contrCuidados.setAnchorView(videoPlayer);

        if (video.compareTo("cuidados")==0){
            videoPath+=R.raw.cuidados;
        }
        else if (video.compareTo("kit")==0){
            videoPath+=R.raw.kit;
        }
        else if (video.compareTo("seguridad")==0){
            videoPath+=R.raw.seguridad;
        }
        else{
            videoPath+=R.raw.seniales;
        }

        Uri uri = Uri.parse(videoPath);
        videoPlayer.setVideoURI(uri);
    }
}
