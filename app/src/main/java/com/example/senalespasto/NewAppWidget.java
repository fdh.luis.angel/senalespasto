package com.example.senalespasto;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider {

    private static final String MyOnClick = "myOnClickTag";
    private static AppWidgetManager awman;
    private static int awid;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        hacerConsulta(context, appWidgetManager, appWidgetId);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            awman = appWidgetManager;
            awid = appWidgetId;

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);

            Intent it = new Intent(context, PypActivity.class);
            PendingIntent configPendingIntent = PendingIntent.getActivity(context, 1, it, 0);
            remoteViews.setOnClickPendingIntent(R.id.img_widg_info, configPendingIntent);
            //----------------------------
            remoteViews.setOnClickPendingIntent(R.id.lbl_widg_placas, getPendingSelfIntent(context, MyOnClick,appWidgetId));
            appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);

            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    static public void hacerConsulta(Context context){

    }

    static public void hacerConsulta(Context context, AppWidgetManager appWidgetManager, int appWidgetId){
        DbManager db = new DbManager(context);
        PyPCalendar pyp = new PyPCalendar();

        db.consultar();
        db.close();

        String info = "";
        for (int i = 0; i < db.getStr_vehiculos().size(); i++) {
            if (pyp.getPyp(db.getStr_placas().get(i)).compareTo("No puede transitar")==0){
                info = "No puede transitar";
                break;
            }
        }

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
        //Poner el pico y placa
        views.setTextViewText(R.id.lbl_widg_placas, pyp.getPyp(true));

        //poner el dia y el mes
        views.setTextViewText(R.id.lbl_widg_dia, pyp.getStrDia() + " " + pyp.getStrFecha());
        views.setTextViewText(R.id.lbl_widg_mes, pyp.getStrMes());

        //SI hay registros info tiene algo y se puede evaluar si un vehiculo tiene restriccion
        //y pone la imagen correspondiente
        if (info.compareTo("No puede transitar")==0){
            views.setImageViewResource(R.id.img_widg_info, R.mipmap.img_widg_mal);
        }
        else{
            views.setImageViewResource(R.id.img_widg_info, R.mipmap.img_widg_bien);
        }
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    protected PendingIntent getPendingSelfIntent(Context context, String action, int idWidget) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action+":"+idWidget);

        //intent.putExtra("idw",idWidget);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (MyOnClick.equals(intent.getAction().split(":")[0])){
            AppWidgetManager apm = AppWidgetManager.getInstance(context);
            hacerConsulta(context,apm,Integer.parseInt(intent.getAction().split(":")[1]));
            Toast.makeText(context,"Pico y placa hoy: " + new PyPCalendar().getPyp(),Toast.LENGTH_SHORT).show();
        }
        super.onReceive(context,intent);
    }
}

