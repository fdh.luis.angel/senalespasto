package com.example.senalespasto;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PypActivity extends AppCompatActivity {
    ListView lista_vehiculos_pyp;
    TextView lbl_placas;
    TextView lbl_dia;
    TextView lbl_mes;
    private PyPCalendar pCalendar;
    private DbManager db;
    private ArrayList<String> ids;
    private ArrayList<String> vehiculos;
    private ArrayList<String> placas;
    private ArrayList<String> alarmas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pyp);
        setTitle("");

        ids = new ArrayList<>();
        vehiculos = new ArrayList<>();
        placas = new ArrayList<>();
        alarmas = new ArrayList<>();


        lbl_placas = (TextView) findViewById(R.id.lbl_pyp_placas);
        lbl_dia = (TextView) findViewById(R.id.lbl_pyp_dia);
        lbl_mes = (TextView) findViewById(R.id.lbl_pyp_mes);


        //Poner la informacion
        pCalendar = new PyPCalendar();
        lbl_placas.setText(pCalendar.getPyp());
        lbl_dia.setText(pCalendar.getStrDia());
        lbl_mes.setText(pCalendar.getStrMes() + " "+ pCalendar.getStrFecha());

        lista_vehiculos_pyp = (ListView)findViewById(R.id.lista_vehiculos_pyp);

        hacerConsulta();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_mas);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent it1 = new Intent(PypActivity.this, EditPypActivity.class);
                Bundle b = new Bundle();
                b.putString("nom_boton", "Cancelar");
                it1.putExtras(b);
                startActivity(it1);
            }
        });
    }

    private void hacerConsulta(){
        //Ejecutar la consulta base de datos
        db = new DbManager(this);
        db.consultar();

        //Inicializar los datos
        ids = db.getStr_ids();
        vehiculos = db.getStr_vehiculos();
        placas = db.getStr_placas();
        alarmas = db.getStr_alarmas();
        //Toast.makeText(this, ,Toast.LENGTH_SHORT).show();


        if (ids.size()>0){

            lista_vehiculos_pyp.setAdapter(new PypAdaptador(this, vehiculos, placas,ids, alarmas));

            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 170, getResources().getDisplayMetrics());

            ViewGroup.LayoutParams params = lista_vehiculos_pyp.getLayoutParams();
            params.height = height*vehiculos.size();
            lista_vehiculos_pyp.setLayoutParams(params);
        }
        db.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ids.clear();

        hacerConsulta();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
    }
}
