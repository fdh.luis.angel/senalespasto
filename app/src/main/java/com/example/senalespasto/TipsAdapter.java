package com.example.senalespasto;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TipsAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    Context contexto;
    String[] datos;
    int[] imagenes;

    public TipsAdapter(Context contexto, String[] datos, int[] imagenes) {
        this.contexto = contexto;
        this.datos = datos;
        this.imagenes = imagenes;
        inflater = (LayoutInflater)contexto.getSystemService(contexto.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        final View vista = inflater.inflate(R.layout.elemento2_lista, null);
        final ImageView imagen = (ImageView) vista.findViewById(R.id.img_tips_element);
        final TextView text =  (TextView) vista.findViewById(R.id.txt_tips_element);
        imagen.setImageResource(imagenes[i]);
        imagen.setTag(i);
        text.setText(datos[i]);

/*
        imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent visorImagen = new Intent(contexto, DescripcionActivity.class);

                Bundle info = new Bundle();
                info.putString("descripcion", datos[(int) v.getTag()]);
                info.putInt("imagen", imagenes[(int) v.getTag()]);
                visorImagen.putExtras(info);
                contexto.startActivity(visorImagen);
            }
        });*/

        return vista;
    }

    @Override
    public int getCount() {
        return imagenes.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
