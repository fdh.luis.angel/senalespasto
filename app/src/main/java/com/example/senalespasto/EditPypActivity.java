package com.example.senalespasto;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import java.util.Date;

import android.content.Intent;
import android.service.autofill.FillEventHistory;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;


public class EditPypActivity extends AppCompatActivity {
    public static int SELECT_TIME_REQUEST = 1;

    NumberPicker numberPicker;
    TextView lbl_placas;
    EditText nom_vehiculo;
    Switch sw_alarma;
    Button btn_action;
    Button btn_guardar;
    TextView lbl_hora;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pyp);

        final PyPCalendar p = new PyPCalendar();
        final DbManager db = new DbManager(this);


        numberPicker = (NumberPicker)findViewById(R.id.num_picker_placa);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(9);

        setTitle(p.getStrMes());

        nom_vehiculo = (EditText)findViewById(R.id.txt_pyp_nom_vehiculo);
        btn_action=(Button) findViewById(R.id.btn_pyp_action);
        btn_guardar=(Button) findViewById(R.id.btn_pyp_guardar);
        lbl_hora = (TextView)findViewById(R.id.lbl_edit_pyp_hora);
        sw_alarma = (Switch)findViewById(R.id.sw_alarma);

        //Recibir el nombre del boton
        final Bundle bundle = getIntent().getExtras();
        final String nom_boton = bundle.getString("nom_boton");

        if (nom_boton.compareTo("Cancelar")==0){
            btn_action.setText("Cancelar");

            btn_guardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (nom_vehiculo.getText().toString().length()>0){
                        db.insertar(nom_vehiculo.getText().toString(),numberPicker.getValue()+"",lbl_hora.getText().toString());
                        Toast.makeText(EditPypActivity.this, "Se agregó "+nom_vehiculo.getText().toString(), Toast.LENGTH_SHORT).show();
                        db.close();
                        finish();
                    }
                    else{
                        Toast.makeText(EditPypActivity.this, "Es necesario un nombre", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btn_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(EditPypActivity.this, "Cancelar :v", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
        else{
            final String id = bundle.getString("id");
            nom_vehiculo.setText(bundle.getString("nom_vehiculo"));

            if (bundle.getString("alarma").length()>1){
                sw_alarma.setChecked(true);
                lbl_hora.setText(bundle.getString("alarma"));
            }

            numberPicker.setValue(Integer.parseInt(bundle.getString("placa")));

            btn_guardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(EditPypActivity.this, "Se creo esa mondá", Toast.LENGTH_SHORT).show();
                    if (nom_vehiculo.getText().toString().length()>0){
                        db.modificar(id,nom_vehiculo.getText().toString(),numberPicker.getValue()+"",lbl_hora.getText().toString());
                        Toast.makeText(EditPypActivity.this, "Se cambió "+nom_vehiculo.getText().toString(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else{
                        Toast.makeText(EditPypActivity.this, "Es necesario un nombre", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btn_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.eliminar(id);
                    Toast.makeText(EditPypActivity.this, "Se eliminó a "+nom_vehiculo.getText().toString(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }

        lbl_placas = (TextView)findViewById(R.id.txt_edit_pyp_placas);

        final CompactCalendarView calendar = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        calendar.setEventIndicatorStyle(1);

        for (int i = 0; i < p.getYearPyp().length; i++) {
            if (nom_boton.compareTo("Cancelar")==0 && p.getCurrPyp()!= -1){
                Event ev1 = new Event(p.getColor(p.getCurrPyp()), p.getEvent(i,p.getCurrPyp()));
                calendar.addEvent(ev1);
            }
            else if(nom_boton.compareTo("Eliminar")==0){
                Event ev1 = new Event(p.getColor(numberPicker.getValue()), p.getEvent(i,numberPicker.getValue()));
                calendar.addEvent(ev1);
            }
        }

        calendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date date) {
                lbl_placas.setText(p.getPyp(date));
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                setTitle(PyPCalendar.translate_month(firstDayOfNewMonth.toString().split(" ")[1]));
            }
        });

        sw_alarma.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Intent alarmView = new Intent(EditPypActivity.this, AlarmaActivity.class);
                    Bundle bAlarm = new Bundle();
                    bAlarm.putString("vehiculo",nom_vehiculo.getText().toString());
                    bAlarm.putInt("placa",numberPicker.getValue());
                    alarmView.putExtras(bAlarm);
                    startActivityForResult(alarmView, SELECT_TIME_REQUEST);
                    //tim.show();
                }
                else lbl_hora.setText(".");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_TIME_REQUEST){
            if (resultCode == RESULT_OK){
                //lbl_hora.setText("15:15");
                Bundle info = data.getExtras();
                if (info != null){
                    lbl_hora.setText(info.getString("hora"));
                }
            }
            else{
                lbl_hora.setText(".");
                sw_alarma.setChecked(false);
            }
        }
        //Toast.makeText(EditPypActivity.this, "ResultCode: "+resultCode, Toast.LENGTH_SHORT).show();
    }
}
