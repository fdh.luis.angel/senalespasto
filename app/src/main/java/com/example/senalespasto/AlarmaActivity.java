package com.example.senalespasto;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class AlarmaActivity extends AppCompatActivity {

    // Cosas de alarma
    PendingIntent pAlarma;
    AlarmManager alarmManager;
    Button btn_on;
    Button btn_off;
    Button btn_salir;
    TextView lbl_info;
    TimePicker timePicker;
    boolean alarma = false;
    int placa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarma);

        final PyPCalendar p = new PyPCalendar();
        placa=-1;

        lbl_info = (TextView)findViewById(R.id.lbl_alarm_info);
        timePicker = (TimePicker)findViewById(R.id.timePickerAlarma);

        //informacion importante
        final Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            placa = bundle.getInt("placa");
            //lbl_info.setText(bundle.getString("vehiculo")+bundle.getInt("placa"));
            //placa = bundle.getInt("placa");
        }

        //Programacion de alarmas
        alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        final Intent iar = new Intent(this, AlarmReceiver.class);


        if (bundle!= null){
            btn_on = (Button)findViewById(R.id.btn_alarm_on);
            btn_on.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int horas = timePicker.getCurrentHour();
                    int minute = timePicker.getCurrentMinute();
                    alarma = true;

                    String minutos = "";
                    if (minute<10){
                        minutos += ("0" + minute);
                    }
                    else{
                        minutos = ""+minute;
                    }
                    Toast.makeText(AlarmaActivity.this, "Sonará a las: "+horas +  ":"+minutos, Toast.LENGTH_SHORT).show();
                    lbl_info.setText(horas +  ":"+minutos);
                    //Set the alarm manager
                    iar.putExtra("extra","alarm on");
                    pAlarma = PendingIntent.getBroadcast(AlarmaActivity.this, 0, iar,PendingIntent.FLAG_UPDATE_CURRENT);

                }
            });
        }


        btn_off = (Button)findViewById(R.id.btn_alarm_off);
        btn_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pAlarma != null){
                    alarmManager.cancel(pAlarma);
                }
                Toast.makeText(AlarmaActivity.this, "Alarma desactivada", Toast.LENGTH_SHORT).show();
                alarma = false;

                iar.putExtra("extra","alarm off");
                //pAlarma = PendingIntent.getBroadcast(AlarmaActivity.this, 0, iar,PendingIntent.FLAG_UPDATE_CURRENT);

                //stop music
                sendBroadcast(iar);
            }
        });

        btn_salir = (Button)findViewById(R.id.btn_alarm_salir);
        btn_salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bundle!=null){
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("hora",lbl_info.getText());
                    if (alarma){
                        setResult(AlarmaActivity.RESULT_OK,returnIntent);
                        Toast.makeText(AlarmaActivity.this, "Suena "+p.getNextPyp(placa), Toast.LENGTH_SHORT).show();
                        //alarmManager.set(AlarmManager.RTC_WAKEUP,p.getNextPypMilis(placa, timePicker.getCurrentHour(), timePicker.getCurrentMinute()),pAlarma);
                        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, p.getNextPypMilis(placa, timePicker.getCurrentHour(), timePicker.getCurrentMinute()),1000*60*10, pAlarma);
                    }
                    else{
                        setResult(AlarmaActivity.RESULT_CANCELED,returnIntent);
                    }
                }
                else
                {
                    iar.putExtra("extra","alarm on");
                    pAlarma = PendingIntent.getBroadcast(AlarmaActivity.this, 0, iar,PendingIntent.FLAG_UPDATE_CURRENT);

                    DbManager db = new DbManager(AlarmaActivity.this);
                    PyPCalendar pyp = new PyPCalendar();

                    db.consultar();
                    db.close();

                    for (int i = 0; i < db.getStr_vehiculos().size(); i++) {
                        if (pyp.getPyp(db.getStr_placas().get(i)).compareTo("No puede transitar")==0){
                            if (db.getStr_alarmas().get(i).length()>1){
                                int loc_hor = Integer.parseInt(db.getStr_alarmas().get(i).split(":")[0]);
                                int loc_min = Integer.parseInt(db.getStr_alarmas().get(i).split(":")[1]);
                                if (loc_hor==pyp.getCurrHour()){
                                    int loc_placa = Integer.parseInt(db.getStr_placas().get(i));
                                    alarmManager.set(AlarmManager.RTC_WAKEUP,p.getNextPypMilis(loc_placa, loc_hor, loc_min),pAlarma);
                                    Toast.makeText(AlarmaActivity.this, "Suena de nuevo "+p.getNextPyp(loc_placa), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                            }
                        }
                    }
                }
                finish();
            }
        });
    }
}
