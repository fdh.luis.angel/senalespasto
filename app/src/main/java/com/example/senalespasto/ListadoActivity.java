package com.example.senalespasto;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ListadoActivity extends AppCompatActivity {

    GridView  grilla_img;
    TextView txt_titulo_listado;
    TextView txt_descripcion_listado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);
        Bundle bundle = getIntent().getExtras();
        setTitle("");

        txt_titulo_listado = (TextView)findViewById(R.id.txt_titulo_listado);
        txt_descripcion_listado = (TextView)findViewById(R.id.txt_descripcion_listado);

        String titulo = bundle.getString("titulo");
        String descripcion = bundle.getString("descripcion");
        int[] imagenes = bundle.getIntArray("imagenes");
        String[] datos = bundle.getStringArray("datos");

        txt_titulo_listado.setText(titulo);
        txt_descripcion_listado.setText(descripcion);

        grilla_img = (GridView)findViewById(R.id.grilla_img);
        grilla_img.setAdapter(new Adaptador(this, datos, imagenes));

    }
}
