package com.example.senalespasto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DescSeguridadActivity extends AppCompatActivity {
    TextView lbl_desc_title;
    ImageView img_desc_seg;
    TextView lbl_info_desc_sec;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desc_seguridad);
        setTitle("Transitic");
        final Bundle bundle = getIntent().getExtras();
        final String titulo = bundle.getString("titulo");
        final int imagen = bundle.getInt("imagen");
        final int info = bundle.getInt("info");

        lbl_desc_title = (TextView)findViewById(R.id.lbl_desc_title);
        lbl_desc_title.setText(titulo);

        img_desc_seg = (ImageView)findViewById(R.id.img_desc_seg);
        img_desc_seg.setImageResource(imagen);

        lbl_info_desc_sec = (TextView)findViewById(R.id.lbl_info_desc_sec);
        lbl_info_desc_sec.setText(info);
    }
}
