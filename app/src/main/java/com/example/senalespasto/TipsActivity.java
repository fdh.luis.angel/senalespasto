package com.example.senalespasto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

public class TipsActivity extends AppCompatActivity {
    ListaDatos datos;
    ImageView btn_tips_moto;
    ImageView btn_tips_carro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);
        setTitle("Tips y recomendaciones");

        btn_tips_carro = (ImageView)findViewById(R.id.btn_tips_carro);
        btn_tips_carro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(TipsActivity.this, DescTipsActivity.class);
                Bundle b = new Bundle();
                b.putInt("titulo", R.string.tit_carros_tips);
                b.putString("vehiculo","carros");
                b.putInt("info",R.string.info_carros_tips);
                it1.putExtras(b);
                startActivity(it1);
            }
        });


        btn_tips_moto = (ImageView)findViewById(R.id.btn_tips_moto);
        btn_tips_moto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(TipsActivity.this, DescTipsActivity.class);
                Bundle b = new Bundle();
                b.putInt("titulo", R.string.tit_motos_tips);
                b.putString("vehiculo","motos");
                b.putInt("info",R.string.info_motos_tips);
                it1.putExtras(b);
                startActivity(it1);
            }
        });
    }
}
