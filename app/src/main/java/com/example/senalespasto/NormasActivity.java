package com.example.senalespasto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class NormasActivity extends AppCompatActivity {
    ImageView btnPeaton;
    ImageView btnConductor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_normas);

        setTitle(R.string.tit_normas);

        btnPeaton = (ImageView) findViewById(R.id.btnPeaton);
        btnPeaton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(NormasActivity.this, PeatonActivity.class);
                startActivity(it1);
            }
        });

        btnConductor = (ImageView) findViewById(R.id.btnConductor);
        btnConductor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it1 = new Intent(NormasActivity.this, ConductorActivity.class);
                startActivity(it1);
            }
        });
    }
}
